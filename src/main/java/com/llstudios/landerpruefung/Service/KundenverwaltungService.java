package com.llstudios.landerpruefung.Service;

import com.llstudios.landerpruefung.Model.Adresse;
import com.llstudios.landerpruefung.Model.Kunde;
import com.llstudios.landerpruefung.Repository.AdresseRepo;
import com.llstudios.landerpruefung.Repository.KundeRepo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor

public class KundenverwaltungService {
    AdresseRepo adresseRepo;
    KundeRepo kundeRepo;

    public List<Adresse> getAllAdresses() {
       return adresseRepo.findAll();
    }

    public List<Adresse> getAllAdressesByLand(String landername){
        return adresseRepo.findAllByLand(landername);
    }


    public Adresse addAdresse(Adresse adresse) {
       return adresseRepo.save(adresse);
    }


    public List<Kunde> getAllKundeFromAdressID(Long id) {
        Optional<Adresse> adresse = adresseRepo.findById(id);
        if (adresse.isPresent()){
            Adresse adresse1 = adresse.get();
            return kundeRepo.findAllByAdresse(adresse1);
        }
        else {
            return null;
        }
    }

    public Adresse updateAdresse(Long id, Adresse adresse){
        Adresse adresseforUpdate = adresseRepo.getOne(id);
        adresseforUpdate.setAdressName(adresse.getAdressName());
        adresseforUpdate.setLand(adresse.getLand());
        return adresseRepo.save(adresseforUpdate);
    }

    public void deleteByID(Long id){
        adresseRepo.deleteById(id);
    }

    public Kunde addKundeToAdress(Long adressId, Kunde kunde) {
        Optional<Adresse> adresse = adresseRepo.findById(adressId);
        if (adresse.isPresent()){
            kunde.setAdresse(adresse.get());
            return kundeRepo.save(kunde);
        }
        else {
            return null;
        }
    }
}
