package com.llstudios.landerpruefung.Controller;

import com.llstudios.landerpruefung.Model.Adresse;
import com.llstudios.landerpruefung.Model.Kunde;
import com.llstudios.landerpruefung.Service.KundenverwaltungService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/api/adresse")
@AllArgsConstructor
public class ControllerRest {

    KundenverwaltungService kundenverwaltungService;

    //post addresse
    @PostMapping
    public ResponseEntity<Adresse> postAdresse(@RequestBody @Valid @NotNull Adresse adresse){
        return ResponseEntity.ok(kundenverwaltungService.addAdresse(adresse));
    }
    //get all Adresses
    @GetMapping
    public ResponseEntity<List<Adresse>> getAllAdresses(){
        return ResponseEntity.ok(kundenverwaltungService.getAllAdresses());
    }

    //get addresse in welchen land
    @GetMapping("land/{land}")
    public ResponseEntity<List<Adresse>> getAllAdressesByLandName(@PathVariable String land){
        return ResponseEntity.ok(kundenverwaltungService.getAllAdressesByLand(land));
    }

    //get all kunde von addresse
    @GetMapping("{id}")
    public ResponseEntity<List<Kunde>> getAllAdressesById(@PathVariable Long id){
        return ResponseEntity.ok(kundenverwaltungService.getAllKundeFromAdressID(id));
    }




    //update Adresse by Id
    @PutMapping("{id}")
    public ResponseEntity<Adresse> updateAdresse(@PathVariable Long id, @RequestBody @Valid @NotNull Adresse adresse){
        return ResponseEntity.ok(kundenverwaltungService.updateAdresse(id,adresse));
    }

    //post {id(adresse}/kunde
    @PostMapping("{id}/kunde")
    public ResponseEntity<Kunde> addKundeToAdressId(@PathVariable Long id, @RequestBody @Valid @NotNull Kunde kunde){
        return ResponseEntity.ok(kundenverwaltungService.addKundeToAdress(id,kunde));
    }



    //delete adresse by ID
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteRestaurantById(@PathVariable Long id){
        kundenverwaltungService.deleteByID(id);
        return ResponseEntity.noContent().build();
    }




}
