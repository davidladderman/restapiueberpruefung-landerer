package com.llstudios.landerpruefung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LandererRestApiUeberprufungLeiterApplication {

    public static void main(String[] args) {
        SpringApplication.run(LandererRestApiUeberprufungLeiterApplication.class, args);
    }

}
