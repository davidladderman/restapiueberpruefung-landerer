package com.llstudios.landerpruefung.Repository;

import com.llstudios.landerpruefung.Model.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdresseRepo extends JpaRepository<Adresse, Long> {
    List<Adresse> findAllByLand(String land);
}
