package com.llstudios.landerpruefung.Repository;

import com.llstudios.landerpruefung.Model.Adresse;
import com.llstudios.landerpruefung.Model.Kunde;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KundeRepo extends JpaRepository<Kunde, Long> {
    List<Kunde> findAllByAdresse(Adresse adresse);
}
