package com.llstudios.landerpruefung.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Kunde {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long kundeId;

    @NotNull
    private String kundenname;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Adresse adresse;
}
